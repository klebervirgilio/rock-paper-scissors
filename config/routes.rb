Rails.application.routes.draw do

  resources :sessions,  only: [:new, :destroy]
  get '/auth/:provider/callback' => "sessions#handle_oauth"

  resources :intro, only: [:index]

  resources :games, only: [:index] do
    get "/play/:throw", action: :play, on: :collection
  end


  root to: 'intro#index'

  # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'
end
