unless Rails.env.test?
  Rails.application.config.middleware.use OmniAuth::Builder do
    provider :developer unless Rails.env.production?
    provider :facebook, Settings.facebook.key, Settings.facebook.secret,
      image_size: :normal, scope: :email
  end
end
