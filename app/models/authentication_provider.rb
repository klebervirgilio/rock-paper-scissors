class AuthenticationProvider < ApplicationRecord

  PROVIDERS = [ FACEBOOK = :facebook ]

  belongs_to :account

  scope :facebook, -> { where(provider: FACEBOOK) }

  validates :uid, :provider, :avatar_url, presence: true
  validates :uid, uniqueness: true
end
