class AccountManager

  def self.find_by_omniauth_or_create!(oauth_params)
    auth = AuthenticationProvider.
      where(uid: oauth_params["uid"]).
      first_or_initialize(
        provider:   oauth_params["provider"],
        avatar_url: oauth_params.dig("info", "image")
      )


    if auth.account.blank?
      Account.create.tap do |account|
        auth.account = account
        auth.save!
      end
    else
      auth.account
    end
  end
end
