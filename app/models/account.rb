class Account < ApplicationRecord
  has_many :authentication_providers
  has_many :facebook_providers, -> { facebook }, through: :authentication_providers
end
