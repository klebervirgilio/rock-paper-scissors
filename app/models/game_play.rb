# This class knows how to plays the game.
class GamePlay

  class InvalidThrowError < StandardError; end

  DEFEAT  = {rock: :scissors, paper: :rock, scissors: :paper}
  THROWS  = DEFEAT.keys
  RESULTS = [ TIE = :tie, WON = :won, LOSE = :lose ]

  attr_reader :player_throw, :on_error

  def initialize(player_throw, &on_error)
    @player_throw = player_throw.to_sym
    @on_error     = on_error
    validate_throw
  end

  # Makes sure the user throw is valid
  def validate_throw
    return if THROWS.include?(player_throw)
    fail InvalidThrowError, "You must throw one of the following: #{THROWS}"
  end

  # finds the winner
  def play
    result, result_message =  if player_throw == computer_throw
                                [TIE, "You tied with the computer. Try again!"]
                              elsif computer_throw == DEFEAT[player_throw]
                                [WON, "Nicely done; #{player_throw} beats #{computer_throw}!"]
                              else
                                [LOSE, "Ouch; #{computer_throw} beats #{player_throw}. Better luck next time!"]
                              end

    {computer_throw: computer_throw, result: result, result_message: result_message}
  end

  # system throw
  def computer_throw
    @computer_throw ||= THROWS.sample
  end
end
