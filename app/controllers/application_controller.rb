class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :require_account

  def current_account
    @account ||= Account.find_by(id: session[:account_id])
  end

  def require_account
    unless current_account
      redirect_to root_path
    end
  end
end
