class SessionsController < ActionController::Base

  def new
    redirect_to "/auth/facebook"
  end

  def handle_oauth
    @account = AccountManager.find_by_omniauth_or_create!(request.env['omniauth.auth'])
    session[:account_id] = @account.id

    if @account
      redirect_to games_path
    else
      redirect_to intro_path
    end
  end

  def destroy
    session[:account_id] = @account = nil
  end
end
