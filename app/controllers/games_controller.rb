class GamesController < ApplicationController

  rescue_from GamePlay::InvalidThrowError do
    render json: {error: 'Invalid throw error'}, status: 422
  end

  def index
  end

  def play
    result = GamePlay.new(params[:throw]).play
    render json: result
  end
end
