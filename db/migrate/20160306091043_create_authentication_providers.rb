class CreateAuthenticationProviders < ActiveRecord::Migration[5.0]
  def change
    create_table :authentication_providers do |t|
      t.string :uid
      t.string :email
      t.string :name
      t.string :avatar_url
      t.string :provider
      t.references :account, index: true, foreign_key: true

      t.timestamps
    end
    add_index :authentication_providers, :uid, unique: true
  end
end
