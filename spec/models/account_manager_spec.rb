require 'rails_helper'

RSpec.describe AccountManager do
  describe '.find_by_omniauth_or_create' do
    subject { AccountManager }
    let(:omniauth_params) do
      {
        "credentials" => {
          "expires" => true,
          "expires_at" => 1462441265,
          "token" => "CAAU5mezYC64BAAC16NeAhKINAIfVLMJFkf2Ggke1Cm6FQzY3Ru2uZAwilFyZADIGdf4ayk7UCyIfTPNU5IIpC7NzpDuSePC7VW0MmH7s0UlRY7ZCOo5bUIZCaVdMN7g9gv7WXGAfojAJLrcTvsOmODdHd0Kvz8hX4fN96hPOyhY38ZA9nXFzQQdC7WgZCZBcPJxGmWscQzSv71G798m4aYz"
        },
        "extra" => {
          "raw_info" => {
            "id" => "10204716305011972",
            "name" => "Kleber Correia"
          }
        },
        "info" => {
          "image" => "http://graph.facebook.com/10204716305011972/picture?type=normal",
          "name" => "Kleber Correia"
        },
        "provider" => "facebook",
        "uid" => "10204716305011972"
      }
    end
    context 'when there is no account for a given provider' do
      it 'creates an account' do
        expect {
          subject.find_by_omniauth_or_create!(omniauth_params)
        }.to change{ Account.count }.by(1)
      end
    end
    context 'when there is an account for a given provider' do
      it 'creates an account' do
        account = Account.create!
        account.authentication_providers.create!(uid: "10204716305011972", provider: :facebook, avatar_url: "url")
        expect(subject.find_by_omniauth_or_create!(omniauth_params)).to eq(account)
      end
    end
  end
end
