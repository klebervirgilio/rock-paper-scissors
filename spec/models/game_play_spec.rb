require 'spec_helper'
require 'pry'
require_relative "../../app/models/game_play"

RSpec.describe GamePlay do
  describe '.play' do

    context 'when invalid throw is given' do
      it "calls block on validation error" do
        expect { GamePlay.new("foo").play}.to raise_error(GamePlay::InvalidThrowError)
      end
    end

    context 'win' do
      before do
        @game = GamePlay.new(:paper)
        allow(@game).to receive(:computer_throw).and_return(:rock)
        @result = @game.play
      end

      it 'retuns proper result code' do
        expect(@result[:result]).to eq(GamePlay::WON)
      end

      it 'retuns proper message' do
        expect(@result[:result_message]).to eq("Nicely done; paper beats rock!")
      end

      it 'retuns computer throw' do
        expect(@result[:computer_throw]).to eq(:rock)
      end
    end

    context 'loss' do
      before do
        @game = GamePlay.new(:rock)
        allow(@game).to receive(:computer_throw).and_return(:paper)
        @result = @game.play
      end

      it 'retuns proper result code' do
        expect(@result[:result]).to eq(GamePlay::LOSE)
      end

      it 'retuns proper message' do
        expect(@result[:result_message]).to eq("Ouch; paper beats rock. Better luck next time!")
      end

      it 'retuns computer throw' do
        expect(@result[:computer_throw]).to eq(:paper)
      end
    end

    context 'tie' do
      before do
        @game = GamePlay.new(:rock)
        allow(@game).to receive(:computer_throw).and_return(:rock)
        @result = @game.play
      end

      it 'retuns proper result code' do
        expect(@result[:result]).to eq(GamePlay::TIE)
      end

      it 'retuns proper message' do
        expect(@result[:result_message]).to eq("You tied with the computer. Try again!")
      end

      it 'retuns computer throw' do
        expect(@result[:computer_throw]).to eq(:rock)
      end
    end
  end
end
