require 'rails_helper'

RSpec.describe GamesController do
  describe "GET play" do

    before do
      # TODO Move to support
      controller.instance_variable_set(:@account, "Account")
    end

    context 'valid requests' do
      subject { get :play, params: {throw: :rock} }
      it 'respond with a JSON' do
        expect(JSON(subject.body)).to_not be_blank
      end

      it 'returns status 200' do
        expect(subject).to be_success
      end
    end
    context 'invalid requests' do
      subject { get :play, params: {throw: :foo} }
      it 'respond with a JSON' do
        expect(JSON(subject.body)).to_not be_blank
      end
      it 'respond with an error message' do
        expect(JSON(subject.body)["error"]).to_not be_blank
      end
      it 'returns status 422' do
        expect(subject.status).to eq(422)
      end
    end
  end
end
