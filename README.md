## Rock Paper Scissors

This is a port from a very old app which I've built for fun a few years ago.
Check it out! https://github.com/klebervirgilio/Rock_Paper_or_Scissors_Game
It was built on top of Rack using sinatra.

This is just the first verion, it's so small and is working fine. 

### Roadmap
- migrate 100% to rails-api
- redo the front-end with emberjs or react
- create real-time matches with real users using websockets action-cable
- port the app to phoinex
- create a elixir/action-cable from scratch
